module Lib
    () where

{-# LANGUAGE OverloadedStrings #-}

import Data.Int
import Data.List as L
import Data.Word
import qualified Data.ByteString as BS
import Data.Text as T
import qualified Data.ByteString.Lazy as LZ

import Data.Binary.Get
import Data.Word

import System.IO

data FlatBufferType = FBByte | FBUByte| FBBool | FBShort | FBUShort | FBInt | FBUInt | FBFloat |
                      FBLong | FBULong | FBDouble | FBAsciiString | FBUTF8String | FBTable [FlatBufferType] | 
                      FlatBufferCompositeType deriving (Show)

data FlatBufferObject = FlatBufferObject [FlatBufferType]

class FlatBuffers a where
    parseFB :: BS.ByteString -> Maybe a

deserialiseRootTableOffset :: Get (Word32)
deserialiseRootTableOffset = do
    offset <- getWord32le
    skip ((fromIntegral offset :: Int) - 4)
    vtableOffset <- getWord32le
    return (offset - vtableOffset)

getWordList16le :: Word16 -> Get ([Word16])
getWordList16le elems = do
    if (elems == 0)
        then return []
        else do b <- getWord16le
                restOfList <- getWordList16le (elems - 1)
                return (b : restOfList)

getStringType :: Get BS.ByteString
getStringType = do
    stringSize <- getWord32le
    getByteString (fromIntegral stringSize :: Int)

--deserialiseVTable :: Get (Word16, Word16, Word16, [Word16], Word32)
deserialiseVTable :: Get (Word16, Word16, Word16, [Word16], BS.ByteString)
deserialiseVTable = do
    sizeVTable <- getWord16le
    totalTableSize <- getWord16le
    let numElems = (sizeVTable - 4) `quot` 2
    tableOffsets <- getWordList16le numElems
    skip $ (fromIntegral (tableOffsets !! 0) :: Int)
    firstElem <- getWord32le
    skip $ ((fromIntegral firstElem :: Int) - 4)
    str <- getStringType
    return (sizeVTable, totalTableSize, numElems, tableOffsets, str)

--loadFBFileAndParse :: FilePath -> IO (Word16, Word16, Word16, [Word16], Word32)
loadFBFileAndParse :: FilePath -> IO (Word16, Word16, Word16, [Word16], BS.ByteString)
loadFBFileAndParse filename = do
    contents <- LZ.readFile filename
    let rootTableOffset = (fromIntegral (runGet deserialiseRootTableOffset contents) :: Int64)
    let (sizeVTable, sizeData, numElems, offsets, firstElem) = runGet deserialiseVTable $ (LZ.drop rootTableOffset contents)
    return (sizeVTable, sizeData, numElems, offsets, firstElem)


