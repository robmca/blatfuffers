def get_u32(f):
    return struct.unpack("<i", f.peek(4)[0:4])[0]

def get_u16(f):
    return struct.unpack("<H", f.peek(2)[0:2])[0]

import sys, struct

filename = sys.argv[1]
f = open(filename, 'rb')

def read_root_vtable_offset(f):
    return get_u32(f)

def read_rel_pos_vtable(f):
    return get_u32(f)

offset = read_root_vtable_offset(f)
f.seek(offset)
rel_pos_vtable = read_rel_pos_vtable(f)
f.seek(f.tell() - rel_pos_vtable)
vtable_size = get_u16(f)
f.seek(f.tell() + 2)
total_vtable_size = get_u16(f)
f.seek(f.tell() + 2)
rel_pos_name = get_u16(f)
f.seek(offset + rel_pos_name)
rel_pos_string = get_u32(f)
f.seek(f.tell() + rel_pos_string)
string_length = get_u32(f)
f.seek(f.tell() + 4)
print(f.read(string_length))

pass
